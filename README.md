# Golang CI image

An image for Golang continuous integration tasks.

## Release notes

  * **1.6.1**
    - go-util v0.8.1
    - Migrated to Gitlab.com
  * **1.6**
    - go-util v0.8
  * **1.5**
    - added github.com/rancher/trash for vendoring
  * **1.4**
    - go-util v0.7
  * **1.3**
    - go-util v0.6
  * **1.2**
    - go-util v0.5
  * **1.1**
    - Better docker tags for git tags
    - Using a fixed version of go-util for repeatability
  * **1.0**
    - Initial release; basic functionality