FROM golang

RUN mkdir /go/src/gitlab.com && ln -s /go/src/gitlab.com /builds

ADD go-util /usr/local/bin

RUN go get -u github.com/rancher/trash && cp /go/bin/trash /usr/local/bin/trash